# -*- coding: utf-8 -*-
from application import app








from web.interceptors.ApiIntercept import *







from web.controllers.index import route_index
from web.controllers.api import route_api
from web.controllers.static import route_static

app.register_blueprint( route_index,url_prefix = "/" )
app.register_blueprint( route_api,url_prefix = "/api")
app.register_blueprint( route_static,url_prefix = "/upload")   #注册路由