# -*- coding: utf-8 -*-
import time  #解决js不自动加载，在前端变量不断在变时js就会重新加载
from application import app
class UrlManager():
    def __init__(ifx):
        pass

    @staticmethod
    def buildUrl( path ):
        return path

    @staticmethod
    def buildStaticUrl(path):
        release_version = app.config.get( 'RELEASE_VERSION' )  #这个每次上线可以更改一下刷新js
        #ver = "%s"%( int( time.time() ) ) if not release_version else release_version #版本号,上线后改回来222222
        ver = "%s"%release_version
                            #如果release_version不存在就执行time.time()否则是release_version
        # 版本号,开发模式实时变动，线上模式人工修改
        path =  "/static" + path + "?ver=" + ver
        return UrlManager.buildUrl( path )

    @staticmethod
    def getupUrl( path ):
        app_config = app.config['APP']
        url = app_config['domain'] + app.config['UPLOAD_RECORD']['prefix_url'] + path
        return url  #由于是给前端使用的所以要返回绝对地址

    @staticmethod
    def getVideoUrl(path):
        app_config = app.config['APP']
        url = app_config['domain'] + app.config['UPLOAD_VIDEO']['prefix_url'] + path
        return url  # 由于是给前端使用的所以要返回绝对地址

    @staticmethod
    def getVideoImgUrl(path):
        app_config = app.config['APP']
        url = app_config['domain'] + app.config['UPLOAD_VIDEO_IMG']['prefix_url'] + path
        return url  # 由于是给前端使用的所以要返回绝对地址

    @staticmethod
    def getRecordUrl(path):
        app_config = app.config['APP']
        url = app_config['domain'] + app.config['UPLOAD_RECORD_S']['prefix_url'] + path
        return url  # 由于是给前端使用的所以要返回绝对地址

    @staticmethod
    def getRecordLiveUrl(path):
        app_config = app.config['APP']
        url = app_config['domain'] + app.config['UPLOAD_RECORD_LIVE']['prefix_url'] + path
        return url  # 由于是给前端使用的所以要返回绝对地址