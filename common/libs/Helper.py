
import datetime


'''
获取当前时间
'''
def getCurrentDate( format = "%Y-%m-%d %H:%M:%S" ):  #参数是时间格式
    return datetime.datetime.now().strftime( format )  #获取当前时间，进行格式化

'''
根据某个字段获取一个dic出来,就是字典
#将某一个结果集按照某一个字段构造成一个字典，key一般都是id
'''
def getDictFilterField( db_model,select_filed,key_field,id_list ):  #1orm数据库，2希望查到哪一个字段，3希望做为字典key字段
                                                                    #4id，这个字段有哪些值
    ret = {} #结果集
    query = db_model.query  #db_model要传递的数据库
    if id_list and len( id_list ) > 0:
        query = query.filter( select_filed.in_( id_list ) )

    list = query.all()  #全部数据
    if not list:
        return ret
    for item in list:
        if not hasattr( item,key_field ):  #hasattr() 函数用于判断对象是否包含对应的属性
            break  #终止循环

        ret[ getattr( item,key_field ) ] = item  #getattr() 函数用于返回一个对象属性值
                    #取出来值直接放进去
    return ret






def getDictListFilterField( db_model,select_filed,key_field,id_list ):
    ret = {}
    query = db_model.query
    if id_list and len( id_list ) > 0:
        query = query.filter( select_filed.in_( id_list ) )

    list = query.all()
    if not list:
        return ret
    for item in list:
        if not hasattr( item,key_field ):
            break
        if getattr( item,key_field ) not in ret:  #取出后判断，如果取出的值不在最终返回结果里面就把它定义成一个list
            ret[getattr(item, key_field)] = []

        ret[ getattr( item,key_field ) ].append(item )  #然后每一次都通过append进行添加

    return ret

'''
从一个对象列表取出某一个字段
'''
def selectFilterObj( obj,field ):
    ret = []
    for item in obj:
        if not hasattr( item,field ):  #如果没有不要了
            continue

        if getattr( item,field ) in ret:  #有也不要了，因为重复了
            continue

        ret.append( getattr( item,field ) )  #添加

    return ret

'''
获取格式化当天时间
'''
def getFormatDate( date = None ,format = "%Y-%m-%d %H:%M:%S"): #默认为空2格式可以自己定义
    if date is None:
        date = datetime.datetime.now()

    return date.strftime( format )
