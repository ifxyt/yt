'''
上传动作统一封装
'''
#from werkzeug.utils import secure_filename
from application import app ,db
import os ,stat ,uuid,datetime
from common.libs.Helper import getCurrentDate
from common.models.Video import Video



class UploadVideo():
    @staticmethod
    def uploadVideoImg( file ,user_id):  # 文件上传
        config_upload = app.config['UPLOAD_VIDEO_IMG']  # 扩展名
        resp = {'code' :200 ,'msg' :"操作成功" ,'data' :{}}

        app.logger.info("org_file:%s" % (file.filename))
        filename = file.filename
        app.logger.info("fm_filename:%s" % (filename))
        name = filename.rsplit(".")

        lens = len(name) -1

        ext = filename.rsplit(".")[lens]


        if ext not in config_upload['ext']:
            resp['code'] = -1
            resp['msg'] = '不允许的扩展'
            return resp

        root_path = app.root_path + config_upload['prefix_path']
            #getCurrentDate()不使用是因为服务器对时间不兼容，保证其他地方可以用
        file_dir = datetime.datetime.now().strftime("%Y%m%d")  # 时间作为文件名,参数是指定日期格式 。年月日
        save_dir = root_path + file_dir
        if not os.path.exists( save_dir ):  # 判断这个路径是否存在
            os.mkdir( save_dir )  # 不存在就直接创建
            os.chmod( save_dir ,stat.S_IRWXU | stat.S_IRGRP | stat.S_IRWXO )  # 给对文件修改等权限,stat.S_IRWXU拥有者有全部权限

            # stat.S_IRGRP用户组有读写权限,其他用户有全部权限(权限掩码)0o007

        file_name = str( uuid.uuid4() ).replace("-" ,"") + "." + ext  # 文件名，uuid根据硬件和时间进行生成一个不重复的字符串
        file.save("{0}/{1}".format( save_dir ,file_name ))  # 存储,路径加文件名

        videoImg = file_dir + "/" + file_name

        model_video_img = Video()
        model_video_img.from_user_id = user_id
        model_video_img.video_img_path = videoImg
        model_video_img.created_time = model_video_img.updated_time = getCurrentDate()
        db.session.add( model_video_img )
        db.session.commit()
        resp["data"] = {
            "key":videoImg
        }

        return resp

    @staticmethod
    def uploadVideo(file,user_id ):  # 文件上传
        config_upload = app.config['UPLOAD_VIDEO']  # 扩展名
        resp = {'code': 200, 'msg': "操作成功", 'data': {}}

        app.logger.info("org_file:%s" % (file.filename))
        filename = file.filename
        app.logger.info("fm_filename:%s" % (filename))
        name = filename.rsplit(".")
        lens = len(name) - 1

        ext = filename.rsplit(".")[lens]

        if ext not in config_upload['ext']:
            resp['code'] = -1
            resp['msg'] = '不允许的扩展'
            return resp

        root_path = app.root_path + config_upload['prefix_path']
        # getCurrentDate()不使用是因为服务器对时间不兼容，保证其他地方可以用
        file_dir = datetime.datetime.now().strftime("%Y%m%d")  # 时间作为文件名,参数是指定日期格式 。年月日
        save_dir = root_path + file_dir
        if not os.path.exists(save_dir):  # 判断这个路径是否存在
            os.mkdir(save_dir)  # 不存在就直接创建
            os.chmod(save_dir, stat.S_IRWXU | stat.S_IRGRP | stat.S_IRWXO)  # 给对文件修改等权限,stat.S_IRWXU拥有者有全部权限

            # stat.S_IRGRP用户组有读写权限,其他用户有全部权限(权限掩码)0o007

        file_name = str(uuid.uuid4()).replace("-", "") + "." + ext  # 文件名，uuid根据硬件和时间进行生成一个不重复的字符串
        file.save("{0}/{1}".format(save_dir, file_name))  # 存储,路径加文件名

        video = file_dir + "/" + file_name

        model_video = Video.query.filter( Video.from_user_id == user_id ).order_by( Video.created_time.desc() ).first()
        model_video.video_path = video
        model_video.updated_time = getCurrentDate()
        db.session.add( model_video )
        db.session.commit()
        resp["data"] = {
            "key": video
        }
        return resp