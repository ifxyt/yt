import hashlib,random,string,requests,json
from application import app



class MemberService():

    @staticmethod
    def geneAuthCode( member_info = None ):   #传递的对象
        '''
        token加密
        :param user_info:
        :return: 加密token
        '''
        m = hashlib.md5()
        str = "%s-%s-%s"%( member_info.id ,member_info.salt ,member_info.status )
        #1,id 2.随机加密码 3.登录状态，加密生成字符
        m.update(str.encode("utf-8"))  # update加密，encode转换成字节码
        return m.hexdigest()  # 转换成16进制编码




#统一产生salt
#大写小写0到9字母

    @staticmethod
    def geneSalt( length = 16 ):  #希望的长度
        keylist = [ random.choice( (string.ascii_letters + string.digits) ) for i in range( length )]
            #返回随机字符串
        return ( "".join( keylist ))

    @staticmethod   #统一获得openid
    def getWechatOpenId( code ):
        url = "https://api.weixin.qq.com/sns/jscode2session?appid={0}&secret={1}" \
              "&js_code={2}&grant_type=authorization_code" \
            .format(app.config['MINA_APP']['appid']
                    , app.config['MINA_APP']['appkey'], code)  # 发送get请求到微信开发服务器
        # 得到openid,sessionkey
        r = requests.get(url)  # 发送get请求

        res = json.loads(r.text)  # loads把字符串转换成json

        # app.logger.info( res )
        openid = None
        if 'openid' in res:
            openid = res['openid']

        return openid  #统一返回