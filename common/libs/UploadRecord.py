'''
上传动作统一封装
'''
#from werkzeug.utils import secure_filename0

from flask import g,jsonify
from application import app ,db
from common.libs.Helper import getCurrentDate
import os ,stat ,uuid,datetime,random
from common.models.TemporaryGetup import TemporaryGetup
from common.models.Record import Record
from common.models.RecordComment import RecordComment
from common.models.User import User


class UploadService():
    @staticmethod
    def uploadGetup(file, from_user_id, sex):  # 文件上传
        config_upload = app.config['UPLOAD_RECORD']  # 扩展名
        resp = {'code' :200 ,'msg' :"操作成功" ,'data' :{}}
        user_info = g.member_info

        filename = file.filename
        name = filename.rsplit(".")
        app.logger.info(name)
        lens = len(name) - 1


        ext = filename.rsplit(".")[lens]

        if ext not in config_upload['ext']:
            resp['code'] = -1
            resp['msg'] = '不允许的扩展'
            return resp

        root_path = app.root_path + config_upload['prefix_path']
            #getCurrentDate()不使用是因为服务器对时间不兼容，保证其他地方可以用
        file_dir = datetime.datetime.now().strftime("%Y%m%d")  # 时间作为文件名,参数是指定日期格式 。年月日
        save_dir = root_path + file_dir
        if not os.path.exists( save_dir ):  # 判断这个路径是否存在
            os.mkdir( save_dir )  # 不存在就直接创建
            os.chmod( save_dir ,stat.S_IRWXU | stat.S_IRGRP | stat.S_IRWXO )  # 给对文件修改等权限,stat.S_IRWXU拥有者有全部权限

            # stat.S_IRGRP用户组有读写权限,其他用户有全部权限(权限掩码)0o007

        file_name = str( uuid.uuid4() ).replace("-" ,"") + "." + ext  # 文件名，uuid根据硬件和时间进行生成一个不重复的字符串
        file.save("{0}/{1}".format( save_dir ,file_name ))  # 存储,路径加文件名
        #domain = app.config["APP"]["domain"] + config_upload["prefix_url"]
        model_record = TemporaryGetup()
        model_record.from_user_id = from_user_id
        model_record.sex = sex
        model_record.record_path = file_dir + "/" + file_name   # 日期加上文件名
        model_record.created_time = model_record.updated_time = getCurrentDate()

        db.session.add( model_record )
        db.session.commit()

        all = TemporaryGetup.query.filter( TemporaryGetup.sex != user_info.sex ).count()
        random_num = random.randint(0,all)

        if random_num < 1:

            all_num = TemporaryGetup.query.count()
            random_int = random.randint(1,all_num)
            key = TemporaryGetup.query.filter_by( id = random_int ).first()
        elif random_num > 1:
            key = TemporaryGetup.query.filter_by( id = random_num ).first()
        else:
            app.logger.info("没有数据")


        resp['data'] = {
            'file': key  # 日期加上文件名

        }
        app.logger.info(key.record_path)
        return resp

    @staticmethod
    def uploadRecord(file, from_user_id ):  # 文件上传
        config_upload = app.config['UPLOAD_RECORD_S']  # 扩展名
        resp = {'code': 200, 'msg': "操作成功", 'data': {}}


        filename = file.filename
        name = filename.rsplit(".")
        app.logger.info(name)
        lens = len(name) - 1
        app.logger.info(lens)
        ext = filename.rsplit(".")[lens]

        if ext not in config_upload['ext']:
            resp['code'] = -1
            resp['msg'] = '不允许的扩展'
            return resp

        root_path = app.root_path + config_upload['prefix_path']
        # getCurrentDate()不使用是因为服务器对时间不兼容，保证其他地方可以用
        file_dir = datetime.datetime.now().strftime("%Y%m%d")  # 时间作为文件名,参数是指定日期格式 。年月日
        save_dir = root_path + file_dir
        if not os.path.exists(save_dir):  # 判断这个路径是否存在
            os.mkdir(save_dir)  # 不存在就直接创建
            os.chmod(save_dir, stat.S_IRWXU | stat.S_IRGRP | stat.S_IRWXO)  # 给对文件修改等权限,stat.S_IRWXU拥有者有全部权限

            # stat.S_IRGRP用户组有读写权限,其他用户有全部权限(权限掩码)0o007

        file_name = str(uuid.uuid4()).replace("-", "") + "." + ext  # 文件名，uuid根据硬件和时间进行生成一个不重复的字符串
        file.save("{0}/{1}".format(save_dir, file_name))  # 存储,路径加文件名

        model_record = Record()
        model_record.from_user_id = from_user_id
        model_record.record_path = file_dir + "/" + file_name  # 日期加上文件名
        model_record.created_time = model_record.updated_time = getCurrentDate()

        db.session.add(model_record)
        db.session.commit()


        resp['data'] = {
            'file_key': file_dir + "/" + file_name  # 日期加上文件名
        }

        return resp

    @staticmethod
    def uploadRecordLive(file, from_user_id,from_record_id,comment ):  # 文件上传
        config_upload = app.config['UPLOAD_RECORD_LIVE']  # 扩展名
        resp = {'code': 200, 'msg': "操作成功", 'data': {}}

        filename = file.filename
        name = filename.rsplit(".")

        lens = len(name) - 1

        ext = filename.rsplit(".")[lens]

        if ext not in config_upload['ext']:
            resp['code'] = -1
            resp['msg'] = '不允许的扩展'
            return resp

        root_path = app.root_path + config_upload['prefix_path']
        # getCurrentDate()不使用是因为服务器对时间不兼容，保证其他地方可以用
        file_dir = datetime.datetime.now().strftime("%Y%m%d")  # 时间作为文件名,参数是指定日期格式 。年月日
        save_dir = root_path + file_dir
        if not os.path.exists(save_dir):  # 判断这个路径是否存在
            os.mkdir(save_dir)  # 不存在就直接创建
            os.chmod(save_dir, stat.S_IRWXU | stat.S_IRGRP | stat.S_IRWXO)  # 给对文件修改等权限,stat.S_IRWXU拥有者有全部权限

            # stat.S_IRGRP用户组有读写权限,其他用户有全部权限(权限掩码)0o007

        file_name = str(uuid.uuid4()).replace("-", "") + "." + ext  # 文件名，uuid根据硬件和时间进行生成一个不重复的字符串
        file.save("{0}/{1}".format(save_dir, file_name))  # 存储,路径加文件名
        # domain = app.config["APP"]["domain"] + config_upload["prefix_url"]
        model_record_live = RecordComment()
        model_record_live.from_user_id = from_user_id
        model_record_live.from_record_id = from_record_id
        model_record_live.comment = comment
        model_record_live.record_path = file_dir + "/" + file_name  # 日期加上文件名
        model_record_live.created_time = model_record_live.updated_time = getCurrentDate()

        db.session.add( model_record_live )
        db.session.commit()

        resp['data'] = {
            'file_key': file_dir + "/" + file_name  # 日期加上文件名
        }

        return resp