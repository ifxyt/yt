# coding: utf-8
from sqlalchemy import Column, DateTime, Index, Integer, String, Text
from sqlalchemy.schema import FetchedValue
from application import db


class RecordComment(db.Model):
    __tablename__ = 'record_comment'
    __table_args__ = (
        db.Index('from_user_id', 'from_user_id', 'from_record_id'),
    )

    id = db.Column(db.Integer, primary_key=True)
    from_user_id = db.Column(db.Integer, nullable=False)
    from_record_id = db.Column(db.Integer, nullable=False)
    comment = db.Column(db.Text)
    record_path = db.Column(db.String(120), nullable=False)
    updated_time = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue())
    created_time = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue())
