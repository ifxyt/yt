# coding: utf-8
from sqlalchemy import Column, DateTime, Integer, String
from sqlalchemy.schema import FetchedValue
from application import db

class Video(db.Model):
    __tablename__ = 'video'

    id = db.Column(db.Integer, primary_key=True)
    from_user_id = db.Column(db.Integer, nullable=False, index=True)
    comment_id = db.Column(db.Integer, nullable=False, server_default=db.FetchedValue())
    content = db.Column(db.String(255), server_default=db.FetchedValue())
    video_path = db.Column(db.String(120), nullable=False, server_default=db.FetchedValue())
    video_img_path = db.Column(db.String(120), nullable=False, server_default=db.FetchedValue())
    like_num = db.Column(db.Integer, nullable=False, server_default=db.FetchedValue())
    updated_time = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue())
    created_time = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue())
