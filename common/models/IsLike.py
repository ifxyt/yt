# coding: utf-8
from sqlalchemy import Column, DateTime, Index, Integer
from sqlalchemy.schema import FetchedValue
from application import db

class IsLike(db.Model):
    __tablename__ = 'is_like'
    __table_args__ = (
        db.Index('from_user_id', 'from_user_id', 'from_video_id', 'status'),
    )

    id = db.Column(db.Integer, primary_key=True)
    from_user_id = db.Column(db.Integer, nullable=False)
    from_video_id = db.Column(db.Integer, nullable=False)
    status = db.Column(db.Integer, nullable=False, server_default=db.FetchedValue())
    updated_time = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue())
    created_time = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue())
