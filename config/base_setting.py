#基础配置公用
SERVER_PORT = 8999   #配置端口
DEBUG = False
SQLALCHEMY_ECHO = False



'''
过滤Url
'''



API_IGNORE_URLS = [
    "^/api"
]



STATUS_MAPPING = {   #搜索状态值
    "1":"正常",
    "0":"已删除",

}

UPLOAD_RECORD = {
    'ext':["aac","mp3"], #扩展文件后缀
    'prefix_path':'/upload/record/getup/',#上传路径前缀
    'prefix_url':'/upload/record/getup/'
}

UPLOAD_RECORD_S = {
    'ext':["aac","mp3"], #扩展文件后缀
    'prefix_path':'/upload/record/record/',#上传路径前缀
    'prefix_url':'/upload/record/record/'
}

UPLOAD_RECORD_LIVE = {
    'ext':["aac","mp3"], #扩展文件后缀
    'prefix_path':'/upload/record/live/',#上传路径前缀
    'prefix_url':'/upload/record/live/'
}


UPLOAD_VIDEO_IMG = {
    'ext':['jpg','gif','bmp','jpeg','png'], #扩展文件后缀
    'prefix_path':'/upload/video/img/',#上传路径前缀
    'prefix_url':'/upload/video/img/'  #前缀

}


UPLOAD_VIDEO = {
    'ext':['mp4','webm'], #扩展文件后缀
    'prefix_path':'/upload/video/path/',#上传路径前缀
    'prefix_url':'/upload/video/path/'   #前缀
}



APP = {  #图片统一域名
    'domain':'http://106.13.15.213:8999'
}

MINA_APP = {
    'appid':'wx3515d6c1e184a4b9',
    'appkey':'1093537ecde7bc2fd93f5d72c5dc224b'
}


