// pages/add/index.js
var app = getApp();
const months = []
const days = []



for (let i = 0; i <= 23; i++) {
  months.push(i)
}

for (let i = 0; i <= 59; i++) {
  days.push(i)
}
var Page = require('../../utils/xmadx_sdk.min.js').xmad(Page).xmPage;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    showAd:true,
    indexChoose:true,
    getupUrl:"",
    cameraCtx:"",
    Camera:"front",
    isCamera:false,
    videoPath:"",
    mainVideoImgPath:"",
    isShow:false,
    txtContent:'',
    isRecord:false,
    recordImg:"/images/add/offRecord.png",
    recordPath:"",
    pikBtnGet:"",
    pikBtnUn:"",
    pikBtnGetNum:0,
    pikBtnUnNum:0,
    getup:false,
    communty:false, 
    recordText:"",
    months: months,
    month: 2,
    days: days,
    day: 2,
    value: [7, 0],
    val:[7,0],
    srcGetup:"",
    xmad: {
      adData: {},
      ad: {
        banner: "xm7c2a48914718a90c76fc124d7aa74a" // 按需引
      }
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.data.cameraCtx = wx.createCameraContext();
    console.log(this.data.cameraCtx)
  },

  
  onShareAppMessage: function () {

  },
  startVideo:function(){
    console.log(456)
    this.data.cameraCtx.startRecord({
      success: function (res) {

      }
    
    });
  },
  stopVideo:function(){
    var that = this;
    console.log(456)
    this.data.cameraCtx.stopRecord({
      success:function( res ){
        
        var path = res.tempVideoPath;
        console.log(path)
        var mainpath = res.tempThumbPath;
        that.setData({
          videoPath:path,
          mainVideoImgPath:mainpath
        });
        
        that.setData({
          isCamera:false,
          isShow:true
        });
      }
    });
  },
  offCamera:function(){
    this.setData({
      isCamera:false,
      indexChoose:true,
      showAd:true
    });
  },
  isVideo:function(){
    this.setData({
      indexChoose:false,
      isCamera:true,
      showAd:false
    });
  },
  cutCamera:function(){
    if ( this.data.Camera == "front"){
      this.setData({
        Camera:"back"
      });
    }else{
      this.setData({
        Camera:"front"
      });
    }
  },
  chooseVideo:function(){
    var that = this;
    wx.chooseVideo({
      sourceType: ['album', 'camera'],
      compressed:false,
      maxDuration: 15,
      
      success(res) {
        var path = res.tempFilePath;
        if (res.duration >= 240 ){
          app.alert({
            'content': "视频只能选择2分钟的哦~"
          });
          that.setData({
            isCamera:true,
            isShow:false
          });
          return;
        }

        
       
        that.setData({
          videoPath:path,
          isCamera:false,
          isShow:true
        });
      }
    });
  },
  issue:function(){
    var that = this;
    var path = [this.data.mainVideoImgPath,this.data.videoPath]
    

    
    if (path[0] == ""){
      var path = "/images/play.png"
      wx.uploadFile({
        url: app.buildUrl("/add/video-img"), // 仅为示例，非真实的接口地址
        filePath: path,
        name: "img",
        header: {
          "content-type": "multipart/form-data",
          "Authorization": app.getCache("token")
        },

        success(res) {
          var resp = res.data


        }
      })
    }else{
      wx.uploadFile({
        url: app.buildUrl("/add/video-img"), // 仅为示例，非真实的接口地址
        filePath: path[0],
        name: "img",
        header: {
          "content-type": "multipart/form-data",
          "Authorization": app.getCache("token")
        },

        success(res) {
          var resp = res.data


        }
      })
    }
    wx.uploadFile({
      url: app.buildUrl("/add/video"), // 仅为示例，非真实的接口地址
      filePath: that.data.videoPath,
      name: "video",
      header: {
        "content-type": "multipart/form-data",
        "Authorization": app.getCache("token")
      },

      success(res) {
        var resp = res.data


      }
    })
    
    wx.request({
      url: app.buildUrl('/add/txt'),
      header: app.getRequestHeader(),
      method: 'POST',
      data: {
        txt: that.data.txtContent
      },
      success: function (res) {
        wx.navigateTo({
          url: '../success/index',
          success: function (res) { },
          fail: function (res) { },
          complete: function (res) { },
        })

        that.setData({
          isShow:false,
          indexChoose:true
        })
      }
    })
  },
  textValue:function(e){
    var a = [];
    var value = e.detail.value;
    var b = a.concat(value);
    var c = b[0];
    
    this.setData({
      txtContent:c
    });
  },                                                             

  recordTextValue:function(e){
    var a = [];
    var value = e.detail.value;
    var b = a.concat(value);
    var c = b[0];

    this.setData({
      recordText: c
    });
  },
  redirectSuc:function(){
    wx.navigateTo({
      url: '/pages/success/index'
    })
  },
  issueRecord:function(){
    var that = this;
    var txt = this.data.recordText
    var file = this.data.recordPath
    wx.uploadFile({
      url: app.buildUrl("/add/record"), // 仅为示例，非真实的接口地址
      filePath: file,
      name: "record",
      header: {
        "content-type": "multipart/form-data",
        "Authorization": app.getCache("token")
      },

      success(res) {
        var resp = res.data


      }
    })
    wx.request({
      url: app.buildUrl('/add/record/txt'),
      header: app.getRequestHeader(),
      method: 'POST',
      data: {
        txt: txt
      },
      success: function (res) {
        wx.navigateTo({
          url: '../success/index',
          success: function (res) { }
        })
        
        that.setData({
          communty:false,
          indexChoose:true
        })
      }
    })
    
  },
  rephotograph:function(){
    this.setData({
      isCamera:true,
      isShow:false
    });
  },
  record:function(){
    this.setData({
      indexChoose:false,
      isRecord:true,
      showAd:false
    });
  },
  startRecord:function(){
     
    var on = "/images/add/onRecord.png";
    this.recorderManager = wx.getRecorderManager();
    const options = {
      duration: 10000,
      sampleRate: 44100,
      numberOfChannels: 1,
      encodeBitRate: 192000,
      format: 'mp3',
      frameSize: 50
    }
    this.recorderManager.start(options);

    this.setData({
      recordImg:on
    });

  },
  stopRecord:function(){
    var that = this;
    this.recorderManager.stop();
    this.recorderManager.onStop(function (res) {
      var recordPath = res.tempFilePath
     
      var off = "/images/add/offRecord.png"
      that.setData({
        recordImg: off,
        recordPath: recordPath
      });
      
    });
    
    
  },
  palyRecord:function(){
    var that = this;
    var innerAudioContext = wx.createInnerAudioContext()
    innerAudioContext.autoplay = true
    innerAudioContext.src = this.data.recordPath
    innerAudioContext.onPlay(() => {
      console.log('开始播放')
    })
    innerAudioContext.onError((res) => {
      console.log(res.errMsg)
      console.log(res.errCode)
    })
  },
  offRecord:function(){
    this.setData({
      isRecord:false,
      indexChoose:true,
      showAd:true
    });
  },
  getup:function(){
   this.setData({
     isRecord:false,
     getup:true
   });
  },
  moneyReward:function(){
    wx.navigateToMiniProgram({
      appId: 'wx18a2ac992306a5a4',
      path: 'pages/apps/largess/detail?id=IDvBAc3%2F0%2F4%3D',
      extraData: {
        foo: 'bar'
      },
      envVersion: 'release',
      success(res) {
        
      }
    })
  },
  retrunUp:function(){
    this.setData({
      communty:false,
      isRecord:true
    });
  },
  communty:function(){
    this.setData({
      isRecord:false,
      communty:true
    });
  },
  retrunRecord:function(){
    this.setData({
      getup:false,
      isRecord:true
    });
  },
  setTime:function( e ){
    console.log(e)
    const val = e.detail.value
    
    this.setData({

      month: this.data.months[val[0]],
      day: this.data.days[val[1]],
      val: val
    })
    
  },
  getUpIssue:function(){
    
    var that = this;
    
    var hour = this.data.val[0]
    var min = this.data.val[1]
    

    

    wx.uploadFile({
      url: app.buildUrl("/add/getup"), // 仅为示例，非真实的接口地址
      filePath: that.data.recordPath,
      name: 'getup',
      header:{
        "content-type": "multipart/form-data",
        "Authorization":app.getCache("token")
      },
      formData: {
        user: 'test'
      },
      success(res) {
        var key = res.data
        console.log(key)
        var obj = JSON.parse(key)
        var geturl = obj.data.key
        var nickname = obj.data.nickname
        var avatar = obj.data.avatar
        
        wx.navigateTo({
          url: '../getUp/index?data=' + geturl + "&hour=" + hour + "&min=" + min + "&avatar=" + avatar + "&nickname=" + nickname
        })

      }
    })
   
    
    
   
  }       
  
});