// pages/getUp/index.js
var Page = require('../../utils/xmadx_sdk.min.js').xmad(Page).xmPage;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    
    h:"",
    m:"",
    s:"",
    time:[],
    src: '',
    nickname:"",
    avatar:"",
    isPlay:false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (e) {
    console.log(e)
    
    this.setData({
      time:[e.hour,e.min],
      src:e.data,
      nickname:e.nickname,
      avatar:e.avatar
    });
    
    
    console.log(e.data)

    var that = this;
    setInterval(function(){
      that.startTime();
      
      that.onBg();
      
      clearInterval()
    },500);
    
    
   
    
    
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  startTime:function ()
{
    var today = new Date()
    var h = today.getHours()
    var m = today.getMinutes()
    var s = today.getSeconds()
    
// add a zero in front of numbers<10
    var m=this.checkTime(m)
    var s=this.checkTime(s)
    this.setData({
      h:h,
      m:m,
      s:s
    });
 
    
  },

checkTime:function (i) {
    if (i < 10) { i = "0" + i }
    return i
  },
  bgMusic:function(){
    var that = this;
    this.data.backgroundAudioManager = wx.getBackgroundAudioManager()
    this.data.backgroundAudioManager.title = "kodaj"
  
    
    // 设置了 src 之后会自动播放
    this.data.backgroundAudioManager.src = that.data.src
  },
  offBgMusic:function(){
    wx.switchTab({
      url: '../index/index'
    })
    
    
    
  },
  onBg:function(){
    var that = this;

    var today = new Date()
    var h = today.getHours()
    var m = today.getMinutes()
    var s = today.getSeconds()
    
    if (h == this.data.time[0] && m == this.data.time[1]&&s==0) {
      that.setData({
        isPlay:true
      })
      if (that.data.isPlay !=false && s < 2 ){
        that.bgMusic();
        that.setData({
          isPlay: false
        })
      }
      
      
    }

    
  }
})