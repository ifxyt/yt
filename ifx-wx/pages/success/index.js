// pages/success/index.js
var Page = require('../../utils/xmadx_sdk.min.js').xmad(Page).xmPage;
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    xmad: {
      adData: {},
      ad: {
        banner: "xm7c2a48914718a90c76fc124d7aa74a" // 按需引
      }
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },


  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
   
  },
  redirect:function(){
    wx.switchTab({
      url: '/pages/index/index'
    })
  }
  
})