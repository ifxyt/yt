var app = getApp();


Page({

  /**
   * 页面的初始数据
   */
  data: {
    btnColor: "",
    loginBtn: false,
    xmad: {
      adData: {},
      ad: {
        banner: "xm7c2a48914718a90c76fc124d7aa74a" // 按需引
      }
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.checkLogin();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  goToIndex: function () {
    wx.switchTab({
      url: '../index/index'
    })
  },
  checkLogin: function () {
    var that = this;
    wx.login({
      success: function (res) {
        if (!res.code) {
          app.alert({ 'content': '登录失败，请再次点击~~' });
          return;
        }
        wx.request({
          url: app.buildUrl('/user/check-reg'),
          header: app.getRequestHeader(),
          method: 'POST',
          data: { code: res.code },
          success: function (res) {
            if (res.data.code != 200) {
              that.setData({
                loginBtn: true  //没有登录
              });
              return;
            }
            app.setCache("token", res.data.data.token);  //缓存
            
            setTimeout(function(){
              that.goToIndex();
            },1800);
            
          }
        });
      }
    });
  },
  login: function (e) {
    var that = this;
    if (!e.detail.userInfo) {
      app.alert({ 'content': '登录失败，请再次点击~~' });
      return;
    }

    var data = e.detail.userInfo;
    wx.login({
      success: function (res) {
        if (!res.code) {
          app.alert({ 'content': '登录失败，请再次点击~~' });
          return;
        }
        data['code'] = res.code;
        wx.request({
          url: app.buildUrl('/user/login'),
          header: app.getRequestHeader(),
          method: 'POST',
          data: data,
          success: function (res) {
            if (res.data.code != 200) {
              app.alert({ 'content': res.data.msg });
              return;
            }
            console.log(res.data.data.token)
            console.log(456)
            app.setCache("token", res.data.data.token);
            that.goToIndex();
          }
        });
      }
    });
  }
})