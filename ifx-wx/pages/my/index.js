// pages/my/index.js

var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    user_id:"489884",
    
    sexImg:"",
    showVideo:true,
    showRecord:false,
    recordSelf:"",
    loadingHidden: false,

    
    recordSelf:"",
    likeSelf:"solid", 
    videoId: 1,
    recordId:1,
    processing: false,
    loadingMoreHidden: true,
    showVideoContent:[],

    likeRecord:[],
    processings: false,
    loadingMoreHiddens: true

  
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
    this.myInfo();
    this.getVideo();
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    if (that.data.showVideo != false ){
      setTimeout(function () {
        that.getVideo();
      }, 500);

    }else{
      setTimeout(function () {
        that.chooseLikeRecord();
      }, 500);
    }
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  redirectRecordInfo:function(e){
    var id = e.currentTarget.dataset.id
    wx.navigateTo({
      url: '../index/recordInfo?id=' + id
    })
  },
  getVideo: function () {
    var that = this;

    if (that.data.processing) {
      return;    //如果为真正在处理终止请求
    }

    if (!that.data.loadingMoreHidden) {  //如果loadingMoreHidden：false没有数据了就终止请求
      return;
    }
    that.setData({
      processing: true
    });  //正在请求
    wx.request({
      url: app.buildUrl("/my/videoList"),
      header: app.getRequestHeader(),
      method: 'POST',
      data: {
        id: that.data.videoId  //分页
      },
      success: function (res) {

        that.setData({
          showVideoContent: that.data.showVideoContent.concat(res.data.data.contentVideo),
          videoId: that.data.videoId + 1,
          processing: false
        });

        if (res.data.data.has_more == 0) {  //判断是否还有食物这个是还有
          that.setData({
            loadingMoreHidden: false
          });
        }
      }
    });
  },
  chooseLikeVideo:function(){
    this.setData({
      showVideo: true,
      showRecord: false
    });

    

  },
  chooseLikeRecord:function(){
   

    var that = this;
    if (that.data.processings) {
      return;    //如果为真正在处理终止请求
    }

    if (!that.data.loadingMoreHiddens) {  //如果loadingMoreHidden：false没有数据了就终止请求
      return;
    }
    that.setData({
      processings: true
    });  //正在请求
    
    wx.request({
      url: app.buildUrl("/my/record"),
      header: app.getRequestHeader(),
      method: 'POST',
      data: {
        id: that.data.recordId  //分页
      },
      success: function (res) {

        that.setData({
          likeRecord: that.data.likeRecord.concat(res.data.data.likeRecord),
          recordId: that.data.recordId + 1,
          processings: false
        });

        if (res.data.data.has_more == 0) {  //判断是否还有食物这个是还有
          that.setData({
            loadingMoreHiddens: false
          });
        }
      }
    });

    this.setData({
      showVideo: false,
      showRecord: true
    });
  },
  redirectInfo:function(e){
    console.log(e)
    var id = e.currentTarget.dataset.id
    wx.navigateTo({
      url: '../index/videoinfo?id=' + id 
    })
  },
  myInfo:function(){
    var that = this;
    wx.request({
      url: app.buildUrl('/my/info'),
      header: app.getRequestHeader(),
      success: function (res) {
        var resp = res.data;
        if (res.data.code != 200) {
          
          return;
        }

        
        var sex = resp.data.sex;
        

        that.setData({
          avatar:resp.data.avatar,
          nickname:resp.data.nickname
        });

        if (sex == 1) {
          that.setData({
            sexImg: "/images/my/boy.png"
          });
        }

        if (sex == 2) {
          that.setData({
            sexImg: "/images/my/girl.png"
          });
        }
      }
    })
  }

})