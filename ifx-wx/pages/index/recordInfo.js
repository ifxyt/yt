// pages/index/recordInfo.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    id:0, //页面id
    recordImg: "/images/index/offRecord.png",
    userAvatar:"",
    userNickname:"",
    selfRecord:[],
    liveUser:[],
    recordLivePath:"",
    recordLiveTxt:"",
    isLike:false,
    lickNumber:0,
    isReplay: true,
    processing: false,
    loadingMoreHidden: true,
    p:1
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (e) {
    var that = this;


    that.setData({
      id: e.id  //赋值id
    });
    this.getRecord();
    this.getMy();
    this.getRecordLive();
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    setTimeout(function () {
      that.getRecordLive();
    }, 500);
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    var that = this;

    return {

      title: that.data.selfRecord[0].headline,
      path: '/pages/index/videoinfo?id=' + that.data.selfRecord[0].id,
      success: function (res) {
        // 转发成功


      },
      fail: function (res) {
        // 转发失败
        }
    }
  },
  playLive:function(e){
    console.log(e)
    var path = e.currentTarget.dataset.path
    var nickname = e.currentTarget.dataset.nickname
    var headline = e.currentTarget.dataset.headline
    this.data.backgroundAudioManager = wx.getBackgroundAudioManager()

    this.data.backgroundAudioManager.title = headline
    
    this.data.backgroundAudioManager.singer = nickname
  
    this.data.backgroundAudioManager.src = path
    

    
  },
  recordPlay: function (e) {

    var path = e.currentTarget.dataset.path
    var nickname = e.currentTarget.dataset.nickname
    var headline = e.currentTarget.dataset.headline
    this.data.backgroundAudioManager = wx.getBackgroundAudioManager()

    this.data.backgroundAudioManager.title = headline

    this.data.backgroundAudioManager.singer = nickname

    this.data.backgroundAudioManager.src = path

  },
  recordLike:function(){
    var that = this;
    wx.request({
      url: app.buildUrl("/record/like"),
      header: app.getRequestHeader(),
      method: 'POST',
      data: {
        id: that.data.id
      },
      success: function (res) {



        

        if (that.data.isLike != true) {
          wx.vibrateShort({})
          that.setData({
            isLike: true
          })
        } else {
          that.setData({
            isLike: false
          })
        }
      }
    });
  },
  getMy:function(){
    var that = this;
    wx.request({
      url: app.buildUrl('/my/info'),
      header: app.getRequestHeader(),
      success: function (res) {
        var resp = res.data;
        if (res.data.code != 200) {

          return;
        }

        that.setData({
          userAvatar: resp.data.avatar,
          userNickname: resp.data.nickname,
        });

      }
    })

  },
  getRecord:function(){
    var that = this;
    wx.request({
      url: app.buildUrl('/record/info'),
      header: app.getRequestHeader(),
      method: 'POST',
      data: {
        id: that.data.id
      },
      success: function (res) {
        var resp = res.data
        if (res.data.code != 200) {

          return;

        }
        var resp = res.data.data;
        that.setData({
          selfRecord: resp.selfRecord
        });
        
      }
    });
  },
  getRecordLive:function(){
    var that = this;
    if (that.data.processing) {
      return;    //如果为真正在处理终止请求
    }

    if (!that.data.loadingMoreHidden) {  //如果loadingMoreHidden：false没有数据了就终止请求
      return;
    }
    that.setData({
      processing: true
    });  //正在请求
    wx.request({
      url: app.buildUrl('/record/liveUser'),
      header: app.getRequestHeader(),
      data:{
        id:that.data.id,
        p:1
      },
      success: function (res) {
        var resp = res.data
        if( resp.code != 200 ){
            return;
        }
        that.setData({
          liveUser: that.data.liveUser.concat(resp.data.liveUser),
          p: that.data.p + 1,
          processing: false
        })
        if (res.data.data.has_more == 0) {  //判断是否还有食物这个是还有
          that.setData({
            loadingMoreHidden: false
          });
        }
      }
    })
  },
  startRecord:function(){
    var on = "/images/index/onRecord.png"
    this.setData({
      recordImg:on
    });
    this.recorderManager = wx.getRecorderManager();
    const options = {
      duration: 10000,
      sampleRate: 44100,
      numberOfChannels: 1,
      encodeBitRate: 192000,
      format: 'aac',
      frameSize: 50
    }
    this.recorderManager.start(options);
  },
  endRecord:function(){
    var that = this;
    var off = "/images/index/offRecord.png"
    this.recorderManager.stop();
    this.recorderManager.onStop(function(res){
      var recordPath = res.tempFilePath
        that.setData({
          recordImg:off,
          recordLivePath:recordPath
        });
    });
  },
  recordPlay:function(){
    var that = this;
    var path = this.data.recordLivePath
    

    const innerAudioContext = wx.createInnerAudioContext()
    innerAudioContext.autoplay = true
    innerAudioContext.src = path
    innerAudioContext.onPlay(() => {
      console.log('开始播放')
    });
    innerAudioContext.onEnded(function () {
    })
    innerAudioContext.onError((res) => {
      console.log(res.errMsg)
      console.log(res.errCode)
    })
  },
  issueLiveRecord:function(){
    if (this.data.recordLiveTxt == "") {
      return;
    }
    var recordPath = this.data.recordLivePath;
    var txt = this.data.recordLiveTxt;
    var id = this.data.id;
    var that = this;
    wx.uploadFile({
      url: app.buildUrl("/record/live"), // 仅为示例，非真实的接口地址
      filePath: recordPath,
      name: "recordLive",
      header: {
        "content-type": "multipart/form-data",
        "Authorization": app.getCache("token")
      },
      formData: {
        id:id,
        txt:txt
      },

      success(res) {
        var resp = res.data

        wx.showToast({
          title: '成功',
          icon: 'success',
          duration: 2000
        })
      }
    })

    that.getRecordLive();
    that.setData({
      isReplay: false
    })
  },
  textValue:function(e){
    
    var txt = e.detail.value
    console.log(txt)
    this.setData({
      recordLiveTxt:txt
    })
  }
})