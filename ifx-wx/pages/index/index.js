// pages/index/index.js

var Page = require('../../utils/xmadx_sdk.min.js').xmad(Page).xmPage;
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    loadingHidden:false, 
    
    xmad: {
      adData: {},
      ad: {
       
        insert: "xm958570a8b769de764ca9df7da7926a", // 按需引⼊
        
      }
    },
    
    headTxtV:"solid",         
    headTxtR:"",
    recordImg:"/images/index/onRecord.png",
    recordNum:0,
    shareId:"",
    share:[],
    contentVideo:[],
    videoId:1,
    recordId:1,
    showVideo:true,
    showRecord:false,
    contentRecord:[],
    shareid:0,
    shareimg:"",
    shareline:"",
    processing: false,
    
    loadingMoreHidden: true,
    processings: false,
    loadingMoreHiddens:true
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getVideo();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
   

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    setTimeout(function () {
      that.getVideo();
    }, 500);  //延时处理 0.5秒
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (res) {
    
    var that = this;
    console.log(res)
    var id = that.data.shareid
    var img = that.data.shareimg
    var line = that.data.shareline
      console.log(img)
      return {
       
        
        title: line,
        path: '/pages/index/videoinfo?id=' + id,
        imageUrl:img,
        success: function (res) {
          // 转发成功


        },
        fail: function (res) {
          // 转发失败
        }
      }
    
  },
  shareMe:function(){
    var that = this;

    var id = this.data.shareId

    wx.request({
      url: app.buildUrl("/index/share"),
      header: app.getRequestHeader(),
      method: 'POST',
      data: {
        id: id //获取带参数url页面
      },
      success: function (res) {

        if (res.data.code != 200) {
          return;
        }
        var resp = res.data
        console.log(resp)
        that.setData({
          shareid: resp.data.id,
          shareimg: resp.data.imageUrl,
          shareline: resp.data.title
        })
         }
    });
  },
  enter:function(){
    this.setData({
      login:false,
      index:true
    });
  },
  
  chooseVideo:function(){
    this.setData({
      headTxtV:"solid",
      headTxtR:"",
      showVideo:true,
      showRecord:false
    });
    this.getVideo();
  },
  chooseRecord:function(){
    this.setData({
      headTxtR: "solid",
      headTxtV:"",
      showVideo:false,
      showRecord:true
    });
    this.showRecord();
  },

  
  playLive: function (e) {
    console.log(e)
    var path = e.currentTarget.dataset.path
    var nickname = e.currentTarget.dataset.nickname
    var headline = e.currentTarget.dataset.headline
    this.data.backgroundAudioManager = wx.getBackgroundAudioManager()

    this.data.backgroundAudioManager.title = headline

    this.data.backgroundAudioManager.singer = nickname

    this.data.backgroundAudioManager.src = path

  },
  redirectVideoInfo: function (e) {
  
    var id = e.currentTarget.dataset.id;
  
    wx.navigateTo({
      url: "/pages/index/videoinfo?id=" + id
    });
  },
  redirectRecord:function(e){
    var id = e.currentTarget.dataset.id;
  
    wx.navigateTo({
      url: '/pages/index/recordInfo?id=' + id 
      
    })
  },
  shareId:function(e){
    var id = e.currentTarget.dataset.id;
  
    this.setData({
      shareId:id
    });
    this.shareMe();
  },
  getVideo:function(){
    var that = this;

    if (that.data.processing) {
      return;    //如果为真正在处理终止请求
    }

    if (!that.data.loadingMoreHidden) {  //如果loadingMoreHidden：false没有数据了就终止请求
      return;
    }
    that.setData({
      processing: true
    });  //正在请求
    wx.request({
      url: app.buildUrl("/index/videoList"),
      header: app.getRequestHeader(),
      method: 'POST',
      data:{
        id:that.data.videoId  //分页
      },
      success: function (res) {

        that.setData({
          contentVideo: that.data.contentVideo.concat(res.data.data.contentVideo),  
          videoId: that.data.videoId + 1,  
          processing: false  
        });  

        if (res.data.data.has_more == 0) {  //判断是否还有食物这个是还有
          that.setData({
            loadingMoreHidden: false
          });
        }
      }
    });
  },
  showRecord:function(){
    var that = this;

    if (that.data.processings) {
      return;    //如果为真正在处理终止请求
    }

    if (!that.data.loadingMoreHiddens) {  //如果loadingMoreHidden：false没有数据了就终止请求
      return;
    }
    that.setData({
      processings: true
    });  //正在请求
    wx.request({
      url: app.buildUrl("/record/recordList"),
      header: app.getRequestHeader(),
      method: 'POST',
      data: {
        id: that.data.recordId  //分页
      },
      success: function (res) {
        var resp = res.data
        
       
        that.setData({
          contentRecord: that.data.contentRecord.concat(resp.data.contentRecord),
          recordId:that.data.recordId + 1,
          processings: false 
        })
        if (res.data.data.has_more == 0) {  //判断是否还有食物这个是还有
          that.setData({
            loadingMoreHiddens: false
          });
        }
      }
    });
  }
})