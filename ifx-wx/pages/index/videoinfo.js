// pages/index/videoinfo.js
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    id: 0,
   
    p:1,
    symbol: "@",
    
    infoVideo: [],

    liveMassageTxt: [],
    userAvatar:"",
    userNickname:"",
    videoLive:"",
    avatar:"",
    nickname:"",
    videopath:"",
    headline:"",
    likeNumber:"",
    mainImg:"",
    videoid:0,
    isLike:false,
    isReplay:true,
    processing: false,
    loadingMoreHidden: true,
    p: 1
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (e) {
    var that = this;


    that.setData({
      id: e.id  //赋值id
    });
    this.videoInfo();
    this.my();
    this.videoLiveMassage();

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    setTimeout(function () {
      that.videoLiveMassage();
    }, 500);  //延时处理 0.5秒
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    var that = this;

    return {

      title: that.data.headline,
      path: '/pages/index/videoinfo?id=' + that.data.videoid,
      imageUrl: that.data.mainImg,
      success: function (res) {
        // 转发成功


      },
      fail: function (res) {
        // 转发失败
      }
    }

  },
  textValue:function( e ){
    console.log(e)
    var txt = e.detail.value;
    this.setData({
      videoLive:txt
    });
    
    console.log(txt)
  },
  my:function(){
    var that = this;
    wx.request({
      url: app.buildUrl('/my/info'),
      header: app.getRequestHeader(),
      success: function (res) {
        var resp = res.data;
        if (res.data.code != 200) {

          return;
        }

        that.setData({
          userAvatar: resp.data.avatar,
          userNickname:resp.data.nickname,
        });

      }
    })

  },
  videoLiveM:function(){
    var that = this;
    if(that.data.isReplay == ""){
      return;
    }
    wx.request({
      url: app.buildUrl('/index/videoaddlive'),
      header: app.getRequestHeader(),
      method: 'POST',
      data: { 
        txt:that.data.videoLive,
        id:that.data.id
       },
      success: function (res) {
        
          wx.showToast({
            title: '成功',
            icon: 'success',
            duration: 2000
          })
        

        that.videoLiveMassage();
        that.setData({
          isReplay:false
        })
      }
    });
  },
  videoInfo:function(){
    var that = this;
    wx.request({
      url: app.buildUrl('/index/videoinfo'),
      header: app.getRequestHeader(),
      method: 'POST',
      data: {
        id: that.data.id
      },
      success: function (res) {
        var resp = res.data
        if (res.data.code != 200) {

          return;

        }
        
        that.setData({
          videoid:resp.data.videoInfo[0].id,
          avatar:resp.data.videoInfo[0].avatar,
          nickname:resp.data.videoInfo[0].nickname,
          videopath: resp.data.videoInfo[0].videoPath,
          headline: resp.data.videoInfo[0].headline,
          likeNumber: resp.data.videoInfo[0].likeNumber,
          liveMassage: resp.data.videoInfo[0].liveMassage,
          mainImg: resp.data.videoInfo[0].mainImg 
        });
        
      }
    });
  },
  videoLiveMassage:function(){
    var that = this;
    if (that.data.processing) {
      return;    //如果为真正在处理终止请求
    }

    if (!that.data.loadingMoreHidden) {  //如果loadingMoreHidden：false没有数据了就终止请求
      return;
    }
    that.setData({
      processing: true
    });  //正在请求
    wx.request({
      url: app.buildUrl('/index/videolive'),
      header: app.getRequestHeader(),
      method: 'POST',
      data: {
        id: that.data.id,
        p:that.data.p
      },
      success: function (res) {
        var resp = res.data
        if (res.data.code != 200) {

          return;

        }

        that.setData({
          liveMassageTxt: that.data.liveMassageTxt.concat(resp.data.video_live),
          videoId: that.data.p + 1,
          processing: false
        });

        if (res.data.data.has_more == 0) {  //判断是否还有食物这个是还有
          that.setData({
            loadingMoreHidden: false
          });
        }
      }
    });
  },
  likeNum: function () {
    var that = this;
    wx.request({
      url: app.buildUrl("/index/like"),
      header: app.getRequestHeader(),
      method: 'POST',
      data: {
        id: that.data.id
      },
      success: function (res) {



        that.setData({
          likeNum: res.data.data.likeNum
        })

        if(that.data.isLike != true){
          wx.vibrateShort({})
          that.setData({
            isLike:true
          })
        }else{
          that.setData({
            isLike:false
          })
        }
      }
    });
  }
})