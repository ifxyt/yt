from web.controllers.api import route_api
from flask import request,jsonify,g #网络请求，json
from application import app,db
from common.libs.Helper import getCurrentDate
from common.models.Video import Video
from common.libs.UrlManager import UrlManager
from common.models.User import User
from common.models.VideoComment import VideoComment
from common.models.IsLike import IsLike
import datetime



@route_api.route("/index/videoList",methods=["POST"])  #授权登录
def video_list():
    resp = {'code': 200, 'msg': '成功', 'data': {}}
    req = request.values
    id = int(req['id']) if 'id' in req else 1  # 页数默认为1
    page_size = 10
    offset = ( id - 1 ) * page_size

    model_video_list = Video.query.order_by( Video.created_time.desc() ).offset( offset ).limit( page_size ).all()

    show_video_list = []

    if model_video_list:
        for item in model_video_list:
            model_user = User.query.filter_by(id=item.from_user_id).first()
            model_video_live = VideoComment.query.filter_by( from_video_id = item.id ).count()
            if len(item.content) < 20:
                txt = item.content
            else:
                txt = item.content[:21] + "..."
            app.logger.info(len(item.content))
            tmp_data = {
                "id": item.id,
                "mainImg": UrlManager.getVideoImgUrl( item.video_img_path ),
                "videoPath": UrlManager.getVideoUrl( item.video_path),
                "headline": txt,
                "avatar":model_user.avatar,
                "nickname":model_user.nickname,
                "likeNumber":item.like_num,
                "liveMassage":model_video_live,
                "time":item.created_time.strftime("%m月%d日%H时%m分")
            }
            show_video_list.append(tmp_data)  # //添加数据

    resp["data"]["contentVideo"] = show_video_list
    resp['data']['has_more'] = 0 if len(show_video_list) < page_size else 1  # 请求到最后一页判断，0是没有，1有
    return jsonify( resp )




@route_api.route("/index/videoinfo" , methods = ["GET","POST"])  #授权登录
def videoInfo():
    resp = {'code': 200, 'msg': '成功', 'data': {}}
    req = request.values
    user_info = g.member_info
    # app.logger.info(req)
    id = req['id'] if 'id' in req else ''
    if not id or len(id) < 1:
        resp['code'] = -1
        resp['msg'] = "没有转发内容"
        return jsonify(resp)

    model_video = Video.query.filter_by( id = id ).first()
    model_video_live = VideoComment.query.filter_by( from_video_id = model_video.id ).count()
    model_user = User.query.filter_by(id=model_video.from_user_id).first()
   # model_islike = IsLike.query.filter_by( from_user_id = user_info.id , from_video_id = id ,status = 1 ).first()

    show_video_info = []
    if model_video:
        tmp_data = {
            "id": model_video.id,
            "mainImg": UrlManager.getVideoImgUrl(model_video.video_img_path),  # "%s-%s"%( item.name , item.id ),
            "videoPath": UrlManager.getVideoUrl(model_video.video_path),
            "headline": model_video.content,
            "avatar": model_user.avatar,
            "nickname": model_user.nickname,
            "likeNumber": model_video.like_num,
            "liveMassage": model_video_live
        }
        show_video_info.append(tmp_data)  # //添加数据


    resp["data"]["videoInfo"] = show_video_info
    return jsonify( resp )

@route_api.route("/index/videolive" , methods = ["GET","POST"])  #授权登录
def videolive():
    resp = {'code': 200, 'msg': '成功', 'data': {}}
    req = request.values
    id = int(req['id']) if 'id' in req else 1  # 页数默认为1
    app.logger.info(id)
    p = int(req['p']) if 'p' in req else 1  # 页数默认为1
    page_size = 10
    offset = (p - 1) * page_size
    model_video_comment = VideoComment.query.filter_by(from_video_id=id).offset(offset).limit(
        page_size).all()

    video_live = []
    if model_video_comment:

        for item in model_video_comment:
            user_info = User.query.filter_by(id=item.from_user_id).first()
            tmp_data = {

                "avatar": user_info.avatar,
                "nickname": user_info.nickname,
                "txt": item.comment,
                "time": item.created_time.strftime("%m月%d日%H时%m分")
            }
            video_live.append(tmp_data)  # //添加数据
    resp['data']['has_more'] = 0 if len(video_live) < page_size else 1
    resp["data"]["video_live"] = video_live

    return jsonify( resp )

@route_api.route("/index/videoaddlive" , methods = ["GET","POST"])  #授权登录
def videoAddLive():
    resp = {"code":200,"msg":"成功","data":{}}
    req = request.values

    user_info = g.member_info
    txt = req["txt"] if 'txt' in req else ''

    app.logger.info(txt)
    if not txt or len( txt ) < 1:
        resp['code'] = -1
        resp['msg'] = "请在输入内容后随意点击屏幕，不然无法获取内容"
        return jsonify(resp)

    id = req["id"] if 'id' in req else 0
    app.logger.info(id)

    model_video_live = VideoComment()
    model_video_live.from_video_id = id
    model_video_live.from_user_id = user_info.id
    model_video_live.comment = txt
    model_video_live.created_time = model_video_live.updated_time = getCurrentDate()

    db.session.add(model_video_live)
    db.session.commit()
    return jsonify( resp )

@route_api.route("/index/like",methods=["POST"])  #授权登录
def videoLike():
    resp = {'code': 200, 'msg': '成功', 'data': {}}
    req = request.values
    user_info = g.member_info
    id = int(req['id']) if 'id' in req else 1  # 页数默认为1
    model_video = Video.query.filter_by( id = id ).first()

    model_video_like = IsLike.query.filter_by( from_user_id = user_info.id,from_video_id = id ).first()
    if not model_video_like:
        model_like = IsLike()
        model_like.from_user_id = user_info.id
        model_like.from_video_id = model_video.id
        model_like.status = 1
        model_like.created_time = model_like.updated_time = getCurrentDate()
        db.session.add(model_like)
        db.session.commit()

        model_video.like_num = model_video.like_num + 1
        db.session.add(model_video)
        db.session.commit()
        app.logger.info(123)
    model_video_like = IsLike.query.filter_by(from_user_id=user_info.id, from_video_id=id).first()
    if model_video_like.status != 1:
        model_video_like.status = 1
        model_video_like.updated_time = getCurrentDate()
        db.session.add(model_video_like)
        db.session.commit()

        model_video.like_num = model_video.like_num - 1
        model_video.updated_time = getCurrentDate()
        db.session.add(model_video)
        db.session.commit()
        app.logger.info(456)
    model_video_like = IsLike.query.filter_by(from_user_id=user_info.id, from_video_id=id).first()
    if model_video_like.status == 1:
        model_video_like.status = 0
        model_video_like.updated_time = getCurrentDate()
        db.session.add(model_video_like)
        db.session.commit()

        model_video.like_num = model_video.like_num + 1
        model_video.updated_time = getCurrentDate()
        db.session.add(model_video)
        db.session.commit()
        app.logger.info(789)
    resp["data"]["likeNum"] = model_video.like_num
    return jsonify( resp )
