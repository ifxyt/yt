from web.controllers.api import route_api
from flask import request,jsonify,g #网络请求，json
from application import app,db
from common.models.Video import Video
from common.models.User import User
from common.models.VideoComment import VideoComment
from common.libs.UrlManager import UrlManager
from common.models.Record import Record
from common.models.RecordComment import RecordComment



@route_api.route("/my/info")

def memberInfo():
    resp = {'code': 200, 'msg': '操作成功~', 'data': {}}
    member_info = g.member_info

    app.logger.info(member_info.avatar)
    resp['data']["avatar"] = member_info.avatar
    resp['data']["sex"] = member_info.sex
    resp['data']["nickname"] = member_info.nickname

    return jsonify(resp)

@route_api.route("/my/videoList",methods=["POST"])  #授权登录
def my_video_list():
    resp = {'code': 200, 'msg': '成功', 'data': {}}
    req = request.values
    user_info = g.member_info
    id = int(req['id']) if 'id' in req else 1  # 页数默认为1
    page_size = 10
    offset = ( id - 1 ) * page_size

    model_video_list = Video.query.filter(Video.from_user_id == user_info.id).order_by( Video.created_time.desc() ).offset( offset ).limit( page_size ).all()

    show_video_list = []

    if model_video_list:
        for item in model_video_list:
            model_user = User.query.filter_by(id=item.from_user_id).first()
            model_video_live = VideoComment.query.filter_by( from_video_id = item.id ).count()
            if len(item.content) < 20:
                txt = item.content
            else:
                txt = item.content[:21] + "..."
            app.logger.info(len(item.content))
            tmp_data = {
                "id": item.id,
                "mainImg": UrlManager.getVideoImgUrl( item.video_img_path ),
                "videoPath": UrlManager.getVideoUrl( item.video_path),
                "headline": txt,
                "avatar":model_user.avatar,
                "nickname":model_user.nickname,
                "likeNumber":item.like_num,
                "liveNumber":model_video_live,
                "time":item.created_time.strftime("%m月%d日%H时%m分")
            }
            show_video_list.append(tmp_data)  # //添加数据

    resp["data"]["contentVideo"] = show_video_list
    resp['data']['has_more'] = 0 if len(show_video_list) < page_size else 1  # 请求到最后一页判断，0是没有，1有
    return jsonify( resp )

@route_api.route("/my/record",methods=["POST"])  #授权登录
def my_record_list():
    resp = {'code': 200, 'msg': '成功', 'data': {}}
    req = request.values
    user_info = g.member_info
    id = int(req['id']) if 'id' in req else 1  # 页数默认为1
    page_size = 10
    offset = ( id - 1 ) * page_size

    model_record_list = Record .query.filter(Record .from_user_id == user_info.id).order_by( Record.created_time.desc() ).offset( offset ).limit( page_size ).all()

    show_record_list = []

    if model_record_list:
        for item in model_record_list:
            model_user = User.query.filter_by(id=item.from_user_id).first()
            model_video_live = RecordComment .query.filter_by( from_record_id = item.id ).count()
            if len(item.content) < 20:
                txt = item.content
            else:
                txt = item.content[:21] + "..."
            app.logger.info(len(item.content))
            tmp_data = {
                "id": item.id,

                "recordPath": UrlManager.getRecordUrl( item.record_path),
                "headline": txt,
                "avatar":model_user.avatar,
                "nickname":model_user.nickname,
                "likeNumber":item.like_num,
                "liveNumber":model_video_live,
                "time":item.created_time.strftime("%m月%d日%H时%m分")
            }
            show_record_list.append(tmp_data)  # //添加数据

    resp["data"]["likeRecord"] = show_record_list
    resp['data']['has_more'] = 0 if len(show_record_list) < page_size else 1  # 请求到最后一页判断，0是没有，1有
    return jsonify( resp )


