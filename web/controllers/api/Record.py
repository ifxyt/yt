from web.controllers.api import route_api
from flask import request,jsonify,g #网络请求，json
from application import app,db
from common.models.Record import Record
from common.models.RecordComment import RecordComment
from common.libs.UrlManager import UrlManager
from common.models.User import User
from common.libs.UploadRecord import UploadService
from common.models.IsLikeRecord import IsLikeRecord
from common.libs.Helper import getCurrentDate
import datetime


@route_api.route("/record/recordList",methods=["POST"])

def recordList():
    resp = {'code': 200, 'msg': '操作成功~', 'data': {}}
    req = request.values


    id = int(req['id']) if 'id' in req else 1  # 页数默认为1
    page_size = 10
    offset = (id - 1) * page_size

    model_video_list = Record.query.order_by(Record.created_time.desc()).offset(offset).limit(page_size).all()

    contentRecord = []

    if model_video_list:
        for item in model_video_list:
            model_user = User.query.filter_by(id=item.from_user_id).first()
            model_record = RecordComment.query.filter_by(from_record_id=item.id).count()

            if len(item.content) < 20:
                txt = item.content
            else:
                txt = item.content[:21] + "..."
            app.logger.info(len(item.content))
            tmp_data = {
                "id": item.id,
                "recordPath": UrlManager.getRecordUrl(item.record_path),
                "avatar": model_user.avatar,
                "headline": txt,
                "nickname": model_user.nickname,
                "likeNumber": item.like_num,
                "liveMassage": model_record,
                "time":item.created_time.strftime("%m月%d日%H时%m分")
            }
            contentRecord.append(tmp_data)  # //添加数据
    resp['data']['has_more'] = 0 if len(contentRecord) < page_size else 1
    resp["data"]["contentRecord"] = contentRecord
    return jsonify(resp)

@route_api.route("/record/info" , methods = ["GET","POST"])  #授权登录
def recordInfo():
    resp = {'code': 200, 'msg': '成功', 'data': {}}
    req = request.values
    # app.logger.info(req)
    id = req['id'] if 'id' in req else ''
    if not id or len(id) < 1:
        resp['code'] = -1
        resp['msg'] = "没有转发内容"
        return jsonify(resp)

    model_record = Record.query.filter_by( id = id ).first()
    model_record_live = RecordComment.query.filter_by( from_record_id = model_record.id ).count()
    model_user = User.query.filter_by(id=model_record.from_user_id).first()

    show_record_info = []
    if model_record:
        tmp_data = {
            "id": model_record.id,
            "recordPath": UrlManager.getRecordUrl(model_record.record_path),
            "headline": model_record.content,
            "avatar": model_user.avatar,
            "nickname": model_user.nickname,
            "lickNumber": model_record.like_num,
            "liveMassage": model_record_live,
            "time":model_record.created_time.strftime("%m月%d日%H时%m分")
        }
        show_record_info.append(tmp_data)  # //添加数据


    resp["data"]["selfRecord"] = show_record_info
    return jsonify( resp )

@route_api.route("/record/live" , methods = ["GET","POST"])  #授权登录
def recordLive():
    resp = {'code': 200, 'msg': '操作成功~', 'data': {}}
    req = request.values
    file = request.files.get("recordLive")
    user_info = g.member_info
    id = int(req['id']) if 'id' in req else 0
    txt = req['txt'] if 'txt' in req else ''

    UploadService.uploadRecordLive(file,user_info.id,id,txt)

    return jsonify( resp )

@route_api.route("/record/liveUser")  #授权登录
def recordReplayLive():
    resp = {'code': 200, 'msg': '操作成功~', 'data': {}}
    req = request.values

    id = int(req['id']) if 'id' in req else 0

    p = int(req['p']) if 'p' in req else 1  # 页数默认为1
    page_size = 10
    offset = (p - 1) * page_size

    model_record_live = RecordComment.query.filter( RecordComment.from_record_id == id ).order_by( RecordComment.created_time.desc()).offset(offset).limit(15).all()

    liveUser = []
    if model_record_live:
        for item in model_record_live:
            user_info = User.query.filter_by( id = item.from_user_id ).first()
            tmp_data = {
                "id":item.id,
                "avatar":user_info.avatar,
                "nickname":user_info.nickname,
                "recordPath":UrlManager.getRecordLiveUrl(item.record_path),
                "liveText":item.comment,
                "time":item.created_time.strftime("%m月%d日%H时%m分")
            }
            liveUser.append(tmp_data)
    resp['data']['has_more'] = 0 if len(liveUser) < page_size else 1
    resp["data"]["liveUser"] = liveUser

    return jsonify( resp )

@route_api.route("/record/like",methods=["POST"])  #授权登录
def recordLike():
    resp = {'code': 200, 'msg': '成功', 'data': {}}
    req = request.values
    user_info = g.member_info
    id = int(req['id']) if 'id' in req else 1  # 页数默认为1
    model_record = Record.query.filter_by( id = id ).first()

    model_record_like = IsLikeRecord.query.filter_by(from_user_id=user_info.id, from_record_id=id).first()
    if not model_record_like:
        model_like = IsLikeRecord()
        model_like.from_user_id = user_info.id
        model_like.from_record_id = model_record.id
        model_like.status = 1
        model_like.created_time = model_like.updated_time = getCurrentDate()
        db.session.add(model_like)
        db.session.commit()

        model_record.like_num = model_record.like_num + 1
        db.session.add(model_record)
        db.session.commit()
        app.logger.info(123)
    model_record_like = IsLikeRecord.query.filter_by(from_user_id=user_info.id, from_record_id=id).first()
    if model_record_like.status != 1:
        model_record_like.status = 1
        model_record_like.updated_time = getCurrentDate()
        db.session.add(model_record_like)
        db.session.commit()

        model_record.like_num = model_record.like_num - 1
        model_record.updated_time = getCurrentDate()
        db.session.add(model_record)
        db.session.commit()
        app.logger.info(456)
    model_record_like = IsLikeRecord.query.filter_by(from_user_id=user_info.id, from_record_id=id).first()
    if model_record_like.status == 1:
        model_record_like.status = 0
        model_record_like.updated_time = getCurrentDate()
        db.session.add(model_record_like)
        db.session.commit()

        model_record.like_num = model_record.like_num + 1
        model_record.updated_time = getCurrentDate()
        db.session.add(model_record)
        db.session.commit()
        app.logger.info(789)
    resp["data"]["likeNum"] = model_record.like_num
    return jsonify( resp )