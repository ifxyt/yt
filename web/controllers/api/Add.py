from web.controllers.api import route_api
from flask import request,jsonify,g #网络请求，json
from application import app,db
from common.libs.UploadRecord import UploadService
from common.libs.UrlManager import UrlManager
from common.models.Video import Video
from common.models.Record import Record
from common.libs.UploadVideo import UploadVideo
from common.libs.Helper import getCurrentDate
from common.models.User import User




@route_api.route("/add/getup",methods = ["POST"])

def getUp():
    resp = {'code': 200, 'msg': '成功', 'data': {}}

    user_info = g.member_info
    file_target = request.files  #文件请求参数

    upfile = file_target['getup'] if 'getup' in file_target else None
    app.logger.info(upfile)



    if upfile is None:  #如果没有
        resp['code'] = -1
        resp['msg'] = "文件不存在"
        return jsonify(resp)

    ret = UploadService.uploadGetup( upfile ,user_info.id ,user_info.sex  )  #上传文件方法



    url = ret['data']['file']


    getUrl = UrlManager.getupUrl(url.record_path)


    model_user = User.query.filter_by(id=url.from_user_id).first()

    resp["data"]["key"] = getUrl
    resp["data"]["nickname"] = model_user.nickname
    resp["data"]["avatar"] = model_user.avatar

    return jsonify ( resp )

@route_api.route("/add/video-img",methods = ["POST"])
def upLoadVideoImg():
    resp = {'code': 200, 'msg': '成功', 'data': {}}

    user_info = g.member_info
    videoPath = request.files.get("img")
    url = UploadVideo.uploadVideoImg(videoPath,user_info.id)

    zurl = url['data']['key']
    app.logger.info(zurl)

    return jsonify( resp )


@route_api.route("/add/video", methods=["POST"])
def upLoadVideo():
    resp = {'code': 200, 'msg': '成功', 'data': {}}

    user_info = g.member_info
    videoPath = request.files.get("video")
    url = UploadVideo.uploadVideo(videoPath,user_info.id)
    zurl = url['data']['key']
    app.logger.info(zurl)



    return jsonify(resp)


@route_api.route("/add/txt", methods=["POST"])
def uploadTxt():
    resp = {'code': 200, 'msg': '成功', 'data': {}}

    user_info = g.member_info
    req = request.values
    comment = req["txt"] if 'txt' in req else ''
    if not comment or len(comment) < 1:
        resp['code'] = -1
        resp['msg'] = "这一刻，说点什么吧..."
        return jsonify(resp)


    app.logger.info(comment)

    model_video = Video.query.filter(Video.from_user_id == user_info.id ).order_by(Video.created_time.desc()).first()
    model_video.content = comment
    model_video.updated_time = getCurrentDate()
    db.session.add( model_video )
    db.session.commit()
    return jsonify(resp)

@route_api.route("/add/record", methods=["POST"])
def uploadRecord():
    resp = {'code': 200, 'msg': '成功', 'data': {}}
    from_user_id = g.member_info
    recordPath = request.files.get("record")
    UploadService.uploadRecord( recordPath, from_user_id.id )
    return jsonify( resp )


@route_api.route("/add/record/txt", methods=["POST"])
def uploadRecordTxt():
    resp = {'code': 200, 'msg': '成功', 'data': {}}

    user_info = g.member_info
    req = request.values
    comment = req["txt"] if 'txt' in req else ''
    if not comment or len(comment) < 1:
        resp['code'] = -1
        resp['msg'] = "这一刻，说点什么吧..."
        return jsonify(resp)


    app.logger.info(comment)

    model_video = Record.query.filter(Record.from_user_id == user_info.id ).order_by(Record.created_time.desc()).first()
    model_video.content = comment
    model_video.updated_time = getCurrentDate()
    db.session.add( model_video )
    db.session.commit()
    return jsonify(resp)
