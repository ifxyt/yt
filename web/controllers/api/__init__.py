from flask import Blueprint
route_api = Blueprint( 'api_page' , __name__ )  #定义一个标识  1.唯一标识

from web.controllers.api.Login import *
from web.controllers.api.Add import *
from web.controllers.api.My import *
from web.controllers.api.index import *
from web.controllers.api.Record import *

@route_api.route("/")
def index():
    return "Mina Api 1"