from web.controllers.api import route_api
from flask import request,jsonify,g #网络请求，json
from application import app,db
from common.libs.MemberService import MemberService
from common.libs.Helper import getCurrentDate
from common.models.User import User
from common.models.UserBind import UserBind



@route_api.route("/user/login" , methods = ["GET","POST"])  #授权登录
def login():
    resp = {'code': 200, 'msg': '成功', 'data': {}}
    req = request.values
    # app.logger.info(req)
    code = req['code'] if 'code' in req else ''
    if not code or len(code) < 1:
        resp['code'] = -1
        resp['msg'] = "需要code"
        return jsonify(resp)

    openid = MemberService.getWechatOpenId(code)  # 获得openid
    if openid is None:
        resp['code'] = -1
        resp['msg'] = "调用微信出错了"
        return jsonify(resp)

    nickname = req['nickName'] if 'nickName' in req else ''
    sex = req['gender'] if 'gender' in req else 0
    avatar = req['avatarUrl'] if 'avatarUrl' in req else ''

    '''
    判断是否已经注册过，注册过直接返回信息
    '''
    bind_info = UserBind.query.filter_by(openid=openid, type=1).first()  # 查询是否绑定判断
    # 是否注册过了，2，信息来源wechat


    if not bind_info:  # 判断是否注册并绑定bind_info

        model_member = User()
        model_member.nickname = nickname
        model_member.sex = sex
        model_member.avatar = avatar
        model_member.salt = MemberService.geneSalt()  # 产生salt
        model_member.updated_time = model_member.created_time = getCurrentDate()
        db.session.add(model_member)
        db.session.commit()

        model_bind = UserBind()
        model_bind.member_id = model_member.id
        model_bind.type = 1
        model_bind.openid = openid
        model_bind.extra = ''
        model_bind.updated_time = model_bind.created_time = getCurrentDate()
        db.session.add(model_bind)
        db.session.commit()

        bind_info = model_bind  # 将model_bind赋值给bind_info以便判断和共member_info调用，统一获取

    member_info = User.query.filter_by(id=bind_info.member_id).first()
    token = "%s#%s" % (MemberService.geneAuthCode(member_info), member_info.id)

    resp['data']["token"] = token
    return jsonify(resp)

@route_api.route("/user/check-reg" , methods = ["GET","POST"])  #不登录
def checkReg():
    resp = {'code': 200, 'msg': '成功', 'data': {}}
    req = request.values
    # app.logger.info(req)
    code = req['code'] if 'code' in req else ''
    if not code or len(code) < 1:
        resp['code'] = -1
        resp['msg'] = "需要code"
        return jsonify(resp)


    # *****************************************共后面代码调用***********************
    openid = MemberService.getWechatOpenId(code)  # 获得openid
    if openid is None:
        resp['code'] = -1
        resp['msg'] = "调用微信出错了"
        return jsonify(resp)

    bind_info = UserBind.query.filter_by(openid=openid, type=1).first()  # 查询是否绑定判断
    if not bind_info:
        resp['code'] = -1
        resp['msg'] = "没有绑定"
        return jsonify(resp)

    member_info = User.query.filter_by(id=bind_info.member_id).first()
    if not member_info:
        resp['code'] = -1
        resp['msg'] = "未查询到绑定信息"
        return jsonify(resp)
    # ***************************************************

    # 不返回用户基本信息，返回加密信息
    token = "%s#%s" % (MemberService.geneAuthCode(member_info), member_info.id)

    resp['data']["token"] = token
    return jsonify(resp)