'''
拦截器Member数据表
'''

from application import app
from flask import request,g,jsonify  #请求，跳转
from common.models.User import User  #数据库
from common.libs.MemberService import MemberService
import re  #正则表达式

@app.before_request
def before_request():  #登录前方法
    api_ignore_urls = app.config['API_IGNORE_URLS']  # get到登录url
    path = request.path   #得到请求地址
    if "/api" not in path:  #如果api不在path里面
        return


    member_info = check_member_login()


    g.member_info = None  #统一全局g变量设置为空
    if member_info:  #若果登录了就是当前用户
        g.member_info = member_info  #如果有就设置为全局的

    #加入访问记录，日志



    pattern = re.compile('%s' % "|".join(api_ignore_urls))
    if pattern.match( path ):
        return


    if not member_info:
        resp = {'code': -1, 'msg': '没有登录', 'data': {}}
        return jsonify( resp )

    return #结束函数


'''
判断用户是否已经登录
'''
def check_member_login():

    auth_cookie = request.headers.get("Authorization")   #获取头部token

    if auth_cookie is None:  #cookie不存在
        return False

    #如果不为空
    auth_info = auth_cookie.split("#")  #以#切割成一个数组，0，授权码1，是uid

    if len( auth_info ) != 2:  #值没有两个0，授权码1，uid
        return False

    try:
        member_info = User.query.filter_by( id = auth_info[1] ).first()  #如果用户输入uid和数据库uid不一样就不存在
    except Exception:
        return False

    if member_info is None:   #如果数据库是空
        return False

    if auth_info[0] != MemberService.geneAuthCode( member_info ):   #请求得到的cookie和加密不一样
        return False

    if member_info.status != 1:  #被删除的账号拦截
        return False


    return member_info  #查询的结果
